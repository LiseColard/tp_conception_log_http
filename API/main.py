from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel


class Mot:
    id:int
    caracteres:str
    def __init__(self,id,caracteres):
        self.id=id
        self.caracteres=caracteres

mots = [Mot(0, 'first'), Mot(1, 'second')]
app = FastAPI()

@app.get("/mot")
async def read_root():
    return mots
    
@app.post("/mots")
async def create_item():
    return mots

